function togglePasswordVisibility(passwordFieldId) {
  var passwordField = document.getElementById(passwordFieldId);
  var passwordIcon = document.getElementById(
    "password-icon" + passwordFieldId.charAt(passwordFieldId.length - 1)
  );

  if (passwordField.type === "password") {
    passwordField.type = "text";
    passwordIcon.src = "show-password.png";
    passwordIcon.alt = "Show Password";
  } else {
    passwordField.type = "password";
    passwordIcon.src = "hide-password.png";
    passwordIcon.alt = "Hide Password";
  }
}

function validatePasswords() {
  var password1 = document.getElementById("password1").value;
  var password2 = document.getElementById("password2").value;
  var errorMessage = document.getElementById("error-message");

  if (password1 === password2) {
    alert("You are welcome");
    errorMessage.textContent = "";
  } else {
    errorMessage.textContent =
      "Passwords do not match. Please enter matching passwords.";
  }
}
